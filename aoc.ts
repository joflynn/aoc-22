import { exit } from "process";
import Days from "./src/days";
import { load } from "./src/utils";

if (process.argv.length < 3) {
  help();
  exit();
}

const command = process.argv[2];
const day = Number(process.argv[3]);
const part = Number(process.argv[4]) || 1;

switch (command) {
  case "day":
    run(day, part);
    break;
  case "test":
    if (!day) {
      testAll();
    } else {
      test(day, part);
    }
    break;
  case "supplemental":
    supplemental(day, part);
    break;
  default:
    help();
}

function run(day: number, part: number = 1) {
  const Day = Days[day];
  if (Day) {
    const input = load(day);
    const result = Day.task(input, part);

    showResult(result);
    if (part === 1 && Day.result1) {
      console.log(`Expected: ${Day.result1}`);
      console.log(result === Day.result1 && "MATCH");
    }
    if (part === 2 && Day.result2) {
      console.log(`Expected: ${Day.result2}`);
      console.log(result === Day.result2 && "MATCH");
    }
  }
}
function test(day: number, part: number = 1): number {
  const Day = Days[day];
  if (Day) {
    const input = Day.testInput;
    const result = Day.task(input, part);

    showResult(result);
    console.log("");

    if (part === 1 && result === Day.testResult1) {
      console.log("PASS");
      return 0;
    }
    if (part === 2 && result === Day.testResult2) {
      console.log("PASS");
      return 0;
    }
  }
  console.log("FAIL");
  return 1;
}
function supplemental(day: number, part: number = 1): number {
  const Day = Days[day];
  if (Day && Day.testSupplemental) {
    console.log("");
    console.log("Supplemental");

    if (Day.testSupplemental(part)) {
      console.log("PASS");
      return 0;
    }
    console.log("FAIL");
    return 1;
  }
  return 0;
}
function testAll() {
  const status = Days.reduce((prev, cur, idx) => {
    if (!cur) {
      return 0;
    }
    console.log("\n---\n\nDay " + idx);
    return prev + test(idx, 1) + test(idx, 2) + supplemental(idx, 2);
  }, 0);
  if (status === 0) {
    console.log("\n---\n\nALL PASSED");
  } else {
    console.log(`\n${status} FAILURES`);
  }
  return status;
}

function help() {
  console.log("Example Usage:");
  console.log("yarn day 14 2   #run day 14, part 2");
  console.log("yarn test 14 2  #run day 14, part 2, with test input");
  console.log("yarn test       #test all");
}

function showResult(result: string) {
  console.log(`\nResult:   ${result}`);
}
