import { ICell } from "../grid";
import { parse } from "../utils";

type SensorCell = ICell & {
  closestBeacon: ICell;
  range?: number;
};

const Y = 10;
//const Y = 2000000;

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  const sensors: SensorCell[] = lines
    .map((l) => {
      const m = l.match(
        /Sensor at x=([-0-9]*), y=([-0-9]*): closest beacon is at x=([-0-9]*), y=([-0-9]*)/
      );
      if (m && m.length > 4) {
        const s: SensorCell = {
          x: Number(m[1]),
          y: Number(m[2]),
          closestBeacon: {
            x: Number(m[3]),
            y: Number(m[4]),
          },
        };
        s.range = manhattan(s, s.closestBeacon);
        return s;
      }
    })
    .filter((x) => x && x.closestBeacon) as SensorCell[];
  const b = bounds(sensors);
  const max = sensors.reduce((acc, cur) => {
    if (cur.range && cur.range > acc) {
      return cur.range;
    }
    return acc;
  }, 0);
  if (part === 1) {
    let sum = 0;
    for (let i = b[0].x - max; i <= b[1].x + max; i += 1) {
      const cur = { x: i, y: Y };
      if (sensors.some((s) => inRange(cur, s))) {
        if (isEmpty(cur, sensors)) {
          //console.log({ x: i, y: Y });
          sum += 1;
        }
      }
    }
    return sum.toString();
  } else {
    let found: ICell = { x: 0, y: 0 };
    sensors.forEach((s) => {
      horizon(s).forEach((p) => {
        //console.log(p);
        if (sensors.every((s) => !inRange(p, s))) {
          if (p.x >= 0 && p.x <= 2 * Y && p.y >= 0 && p.y <= 2 * Y) {
            found = p;
          }
        }
      });
    });
    return (found && (found.x * 4000000 + found.y).toString()) || "";
  }
};

const bounds = (ss: SensorCell[]): [ICell, ICell] => {
  const tl = {
    x: Infinity,
    y: Infinity,
  };
  const br = {
    x: -Infinity,
    y: -Infinity,
  };
  const points = ss.flatMap((s) => [s, s.closestBeacon]);
  points.forEach((cell) => {
    if (cell.x < tl.x) {
      tl.x = cell.x;
    }
    if (cell.x > br.x) {
      br.x = cell.x;
    }
    if (cell.y < tl.y) {
      tl.y = cell.y;
    }
    if (cell.y > br.y) {
      br.y = cell.y;
    }
  });

  return [tl, br];
};
const inRange = (pos: ICell, sensor: SensorCell): boolean => {
  const r = sensor.range || 1;
  return manhattan(pos, sensor) <= r;
};
const manhattan = (s: ICell, e: ICell): number => {
  if (s && e) {
    return Math.abs(s.x - e.x) + Math.abs(s.y - e.y);
  }
  return Infinity;
};
const isEmpty = (pos: ICell, sensors: SensorCell[]): boolean => {
  const same = (s: ICell) => s.x === pos.x && s.y === pos.y;

  return !sensors.some((s) => /* same(s) || */ same(s.closestBeacon));
};
const horizon = (s: SensorCell): ICell[] => {
  const cells: ICell[] = [];
  if (s.range) {
    const start = s.x - s.range - 1;
    const end = s.x + s.range + 1;
    for (let x = start; x <= end; x += 1) {
      const dx = Math.min(x - start, end - x);
      cells.push({ x, y: s.y + dx });
      cells.push({ x, y: s.y - dx });
    }
  }
  return cells;
};

const Day15: Day = {
  task,

  testInput: `Sensor at x=2, y=18: closest beacon is at x=-2, y=15
  Sensor at x=9, y=16: closest beacon is at x=10, y=16
  Sensor at x=13, y=2: closest beacon is at x=15, y=3
  Sensor at x=12, y=14: closest beacon is at x=10, y=16
  Sensor at x=10, y=20: closest beacon is at x=10, y=16
  Sensor at x=14, y=17: closest beacon is at x=10, y=16
  Sensor at x=8, y=7: closest beacon is at x=2, y=10
  Sensor at x=2, y=0: closest beacon is at x=2, y=10
  Sensor at x=0, y=11: closest beacon is at x=2, y=10
  Sensor at x=20, y=14: closest beacon is at x=25, y=17
  Sensor at x=17, y=20: closest beacon is at x=21, y=22
  Sensor at x=16, y=7: closest beacon is at x=15, y=3
  Sensor at x=14, y=3: closest beacon is at x=15, y=3
  Sensor at x=20, y=1: closest beacon is at x=15, y=3`,

  testResult1: "26",
  testResult2: "56000011",

  result1: "4793062",
  result2: "10826395253551",
};
export { Day15 };
