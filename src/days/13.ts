import { parse } from "../utils";

type Pair = {
  index: number;
  left: any;
  right: any;
};
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  const pairs: Pair[] = [];
  let index = 0;
  while (lines.length) {
    index += 1;
    const left = JSON.parse(lines.shift() || "");
    const right = JSON.parse(lines.shift() || "");
    lines.shift();
    pairs.push({ index, left, right });
  }
  //2 false expect true
  //3 true expect false
  //4 false expect true

  if (part === 1) {
    // return pairs
    //   .map((x) => {
    //     console.log(x.index);
    //     console.log(JSON.stringify(x.left));
    //     console.log(JSON.stringify(x.right));
    //     const y = inCorrectOrder(x.left, x.right);
    //     console.log(y);
    //     return y;
    //   })
    //   .toString();

    return pairs
      .reduce(
        (acc, cur) =>
          acc + (inCorrectOrder(cur.left, cur.right) ? cur.index : 0),
        0
      )
      .toString();
  } else {
    return "";
  }
};

const inCorrectOrder = (left: any, right: any): boolean | undefined => {
  //if (typeof left === "object" && typeof right === "object")
  const l = left.shift();
  const r = right.shift();
  console.log({ l, r });
  if (!l && !r) {
    return;
  }
  if (l === undefined) {
    return true;
  }
  if (r === undefined) {
    return false;
  }
  if (typeof l === "number" && typeof r === "number") {
    if (l < r) {
      return true;
    }
    if (l > r) {
      return false;
    }
    return inCorrectOrder(left, right);
  }
  if (typeof l === "object" && typeof r === "number") {
    const check = inCorrectOrder(l, [r]);
    if (check === undefined) {
      return inCorrectOrder(left, right);
    }
    return check;
  }
  if (typeof l === "number" && typeof r === "object") {
    const check = inCorrectOrder([l], r);
    if (check === undefined) {
      return inCorrectOrder(left, right);
    }
    return check;
  }
  if (typeof l === "object" && typeof r === "object") {
    const check = inCorrectOrder(l, r);
    if (check === undefined) {
      return inCorrectOrder(left, right);
    }
    return check;
  }
  return false;
};

const Day13: Day = {
  task,

  testInput: `[1,1,3,1,1]
  [1,1,5,1,1]

  [[1],[2,3,4]]
  [[1],4]

  [9]
  [[8,7,6]]

  [[4,4],4,4]
  [[4,4],4,4,4]

  [7,7,7,7]
  [7,7,7]

  []
  [3]

  [[[]]]
  [[]]

  [1,[2,[3,[4,[5,6,7]]]],8,9]
  [1,[2,[3,[4,[5,6,0]]]],8,9]`,

  testResult1: "13",
  testResult2: "",

  result1: "",
  result2: "",
};
export { Day13 };
