import { parse } from "../utils";

const ROCK = "A";
const PAPER = "B";
const SCISSORS = "C";

const ENC_ROCK = "X";
const ENC_PAPER = "Y";
const ENC_SCISSORS = "Z";

const OUTCOME_LOSE = "X";
const OUTCOME_DRAW = "Y";
const OUTCOME_WIN = "Z";

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((l) => l.trim());
  let score = 0;
  lines.forEach((l) => {
    const [you, me] = l.split(" ");
    let cur;
    if (part === 1) {
      cur = outcomeScore(you, decode(me)) + shapeScore(me);
    } else {
      const me_actual = deduce(you, me);
      cur = outcomeScore(you, me_actual) + shapeScore(me_actual);
    }
    score += cur;
  });
  return score.toString();
};

function deduce(you: string, outcome: string): string {
  switch (outcome) {
    case OUTCOME_WIN:
      return win(you);
    case OUTCOME_DRAW:
      return draw(you);
    case OUTCOME_LOSE:
      return lose(you);
    default:
      return "";
  }

  function win(you: string): string {
    switch (you) {
      case ROCK:
        return PAPER;
      case PAPER:
        return SCISSORS;
      case SCISSORS:
        return ROCK;
      default:
        return "";
    }
  }
  function draw(you: string): string {
    return you;
  }
  function lose(you: string): string {
    switch (you) {
      case ROCK:
        return SCISSORS;
      case PAPER:
        return ROCK;
      case SCISSORS:
        return PAPER;
      default:
        return "";
    }
  }
}
function decode(me: string): string {
  switch (me) {
    case ENC_ROCK:
      return ROCK;
    case ENC_PAPER:
      return PAPER;
    case ENC_SCISSORS:
      return SCISSORS;
    default:
      return "";
  }
}
function outcomeScore(you: string, me: string): number {
  if (you === me) {
    return 3;
  }
  if (you === ROCK && me === PAPER) {
    return 6;
  }
  if (you === PAPER && me === SCISSORS) {
    return 6;
  }
  if (you === SCISSORS && me === ROCK) {
    return 6;
  }
  return 0;
}
function shapeScore(shape: string): number {
  switch (shape) {
    case ENC_ROCK:
    case ROCK:
      return 1;
    case ENC_PAPER:
    case PAPER:
      return 2;
    case ENC_SCISSORS:
    case SCISSORS:
      return 3;
    default:
      return 0;
  }
}

const Day2: Day = {
  task,

  testInput: `A Y
  B X
  C Z`,

  testResult1: "15",
  testResult2: "12",

  result1: "11603",
  result2: "12725",
};
export { Day2 };
