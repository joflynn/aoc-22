import { parseCli } from "../command";
import { parse } from "../utils";

// type Node = {
//   path: string;
//   name: string;
//   size?: number;
//   children?: Node[];
// };

type Dir = {
  path: string;
  size: number;
};
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  //parse
  const commands = parseCli(lines);

  //execute
  const dirs: Dir[] = [];
  let pwd: string[] = [];
  commands.forEach((cmd) => {
    const cd = cmd.input.match(/cd (.*)/);
    const ls = cmd.input.match(/ls/);
    if (cd) {
      const arg = cd[1];
      if (arg === "..") {
        if (pwd.length) {
          pwd.pop();
        }
      } else if (arg === "/") {
        pwd = [];
      } else {
        pwd.push(arg);
      }
    } else if (ls) {
      let sum = 0;

      cmd.output.forEach((listing) => {
        if (listing.match(/^dir/)) {
          //ignore
        } else {
          const match = listing.match(/([0-9]*) (.*)/);
          if (match && match.length > 2) {
            const size = match[1];
            const name = match[2];
            sum += Number(size);
          }
        }
      });

      const dir = {
        path: pwd.join("/"),
        size: sum,
      };
      dirs.push(dir);
    }
  });

  if (part === 1) {
    let sum = 0;
    dirs.forEach((dir) => {
      const size = dirSize(dir.path, dirs);
      if (size < 100000) {
        sum += size;
      }
    });
    return sum.toString();
  } else {
    const totalSize = 70000000;
    const required = 30000000;
    const used = dirSize("", dirs);
    const free = totalSize - used;
    const target = required - free;

    //find smallest directory bigger than target
    let min = 9999999999999;
    dirs.forEach((dir) => {
      const size = dirSize(dir.path, dirs);
      if (size > target) {
        if (size < min) {
          min = size;
        }
      }
    });
    return min.toString();
  }
};

const dirSize = (path: string, dirs: Dir[]) => {
  const all = dirs.filter((d) => d.path.startsWith(path));
  return all.reduce((acc, cur) => acc + cur.size, 0);
};
const Day7: Day = {
  task,

  testInput: `$ cd /
  $ ls
  dir a
  14848514 b.txt
  8504156 c.dat
  dir d
  $ cd a
  $ ls
  dir e
  29116 f
  2557 g
  62596 h.lst
  $ cd e
  $ ls
  584 i
  $ cd ..
  $ cd ..
  $ cd d
  $ ls
  4060174 j
  8033020 d.log
  5626152 d.ext
  7214296 k`,

  testResult1: "95437",
  testResult2: "24933642",

  result1: "1543140",
  result2: "1117448",
};
export { Day7 };
