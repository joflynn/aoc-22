import { parse } from "../utils";

type Cmd = {
  op: "addx" | "noop";
  arg?: number;
  delay: number;
};
type State = {
  x: number;
  samples: number[];
};
type State2 = State & {
  display: string[][];
};

const DELAYS = {
  noop: 1,
  addx: 2,
};
const task: DayFunc = (input, part = 1) => {
  const program = parse(input)
    .map((x) => x.trim())
    .map((line) => {
      const parts = line.split(" ");
      if (parts.length > 0) {
        const op = parts[0];
        if (op === "addx" || op === "noop") {
          return {
            op,
            arg: Number(parts[1]) || undefined,
            delay: DELAYS[op],
          };
        }
      }
    })
    .filter((x) => x);

  if (part === 1) {
    let state = {
      x: 1,
      samples: [],
    };

    execute(state, program as Cmd[], doCmd, sample);
    return state.samples.reduce((acc, cur) => acc + cur, 0).toString();
  } else {
    const state = {
      x: 1,
      samples: [],
      display: Array.from({ length: 6 }, () =>
        Array.from({ length: 40 }, () => ".")
      ),
    };

    const EPSILON = 1;
    execute(state, program as Cmd[], doCmd, (state: State2, cycle: number) => {
      const x = (cycle - 1) % 40;
      const y = Math.floor((cycle - 1) / 40);
      if (Math.abs(state.x - x) <= EPSILON) {
        state.display[y][x] = "#";
      }
    });

    return "\n" + state.display.map((line) => line.join("")).join("\n");
  }
};

function execute<T>(
  state: T,
  program: Cmd[],
  opHandler: (state: T, cmd: Cmd) => void,
  sampleHandler: (state: T, cycle: number) => void
) {
  let current = program.shift();
  let t = 1;
  while (current) {
    current.delay -= 1;
    //during
    sampleHandler(state, t);
    //console.log(t, current, state);
    //after
    if (current.delay <= 0) {
      opHandler(state, current as Cmd);
      current = program.shift();
    }
    t += 1;
  }
}

function doCmd(state: State, cmd: Cmd) {
  switch (cmd.op) {
    case "addx":
      if (cmd.arg) {
        state.x += cmd.arg;
      }
      return;
  }
}

function sample(state: State, cycle: number) {
  if ((cycle - 20) % 40 === 0) {
    //console.log({ state, cycle });
    state.samples.push(state.x * cycle);
  }
}
const Day10: Day = {
  task,

  testInput: /*`noop
  addx 3
  addx -5`,
  */ `addx 15
  addx -11
  addx 6
  addx -3
  addx 5
  addx -1
  addx -8
  addx 13
  addx 4
  noop
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx 5
  addx -1
  addx -35
  addx 1
  addx 24
  addx -19
  addx 1
  addx 16
  addx -11
  noop
  noop
  addx 21
  addx -15
  noop
  noop
  addx -3
  addx 9
  addx 1
  addx -3
  addx 8
  addx 1
  addx 5
  noop
  noop
  noop
  noop
  noop
  addx -36
  noop
  addx 1
  addx 7
  noop
  noop
  noop
  addx 2
  addx 6
  noop
  noop
  noop
  noop
  noop
  addx 1
  noop
  noop
  addx 7
  addx 1
  noop
  addx -13
  addx 13
  addx 7
  noop
  addx 1
  addx -33
  noop
  noop
  noop
  addx 2
  noop
  noop
  noop
  addx 8
  noop
  addx -1
  addx 2
  addx 1
  noop
  addx 17
  addx -9
  addx 1
  addx 1
  addx -3
  addx 11
  noop
  noop
  addx 1
  noop
  addx 1
  noop
  noop
  addx -13
  addx -19
  addx 1
  addx 3
  addx 26
  addx -30
  addx 12
  addx -1
  addx 3
  addx 1
  noop
  noop
  noop
  addx -9
  addx 18
  addx 1
  addx 2
  noop
  noop
  addx 9
  noop
  noop
  noop
  addx -1
  addx 2
  addx -37
  addx 1
  addx 3
  noop
  addx 15
  addx -21
  addx 22
  addx -6
  addx 1
  noop
  addx 2
  addx 1
  noop
  addx -10
  noop
  noop
  addx 20
  addx 1
  addx 2
  addx 2
  addx -6
  addx -11
  noop
  noop
  noop`,
  testResult1: "13140",
  testResult2:
    "\n##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....",

  result1: "13920",
  result2:
    "\n####..##..#....#..#.###..#....####...##.\n#....#..#.#....#..#.#..#.#....#.......#.\n###..#....#....####.###..#....###.....#.\n#....#.##.#....#..#.#..#.#....#.......#.\n#....#..#.#....#..#.#..#.#....#....#..#.\n####..###.####.#..#.###..####.#.....##..",
};
export { Day10 };
