import { parse } from "../utils";

type Stacks = {
  [key: string]: string[];
};

type CrateMoverCommand = {
  count: number;
  from: string;
  to: string;
};

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input);

  //Tricky input format here, stacks formed in columns above a row of indicies.
  //find the index row, grab the labels, then lookup their positions, and
  //then build the stacks by looking above in those positions

  const stacks: Stacks = {};
  const crates: string[] = [];
  let line = lines.shift();
  while (line?.match(/\[.*\]/)) {
    crates.push(line);
    line = lines.shift();
  }

  const indicies = (line || "")
    .split(" ")
    .map((x) => x.trim())
    .filter((x) => x);

  indicies?.forEach((x) => {
    stacks[x] = [];
    const pos = line?.indexOf(x);
    crates.forEach((crate) => {
      if (pos) {
        const cargo = crate[pos];
        if (cargo.trim() !== "") {
          stacks[x].push(cargo);
        }
      }
    });
  });
  //use shift and unshift, stacks are in reverse order

  const commands: CrateMoverCommand[] = [];
  lines
    .filter((x) => x.match(/move/))
    .forEach((instruction) => {
      const command = parseCrateMoverCommand(instruction);
      if (command) {
        commands.push(command);
      }
    });
  if (part === 1) {
    commands.forEach((cmd) => {
      for (let i = 0; i < cmd.count; i += 1) {
        const x = stacks[cmd.from].shift();
        if (x) {
          stacks[cmd.to].unshift(x);
        }
      }
    });
    return topsOfStacks(indicies, stacks);
  } else {
    commands.forEach((cmd) => {
      const buf = [];
      for (let i = 0; i < cmd.count; i += 1) {
        const x = stacks[cmd.from].shift();
        if (x) {
          buf.push(x);
        }
      }
      while (buf.length) {
        const x = buf.pop();
        if (x) {
          stacks[cmd.to].unshift(x);
        }
      }
    });
    return topsOfStacks(indicies, stacks);
  }
};

const parseCrateMoverCommand = (
  command: string
): CrateMoverCommand | undefined => {
  const match = command.match(/move (.*) from (.*) to (.*)/);
  if (match && match.length >= 3) {
    const count = Number(match[1]);
    const from = match[2];
    const to = match[3];

    return { count, from, to };
  }
};
const topsOfStacks = (indicies: string[], stacks: Stacks): string => {
  return indicies?.map((i) => stacks[i][0]).join("") || "";
};

const Day5: Day = {
  task,

  testInput: `    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 
  
move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`,

  testResult1: "CMZ",
  testResult2: "MCD",

  result1: "TWSGQHNHL",
  result2: "JNRSCDWPP",
};
export { Day5 };
