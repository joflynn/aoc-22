import { Grid, HeightCell, parseCharGrid } from "../grid";
import { parse } from "../utils";

type HeightGrid = Grid<HeightCell>;
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  const grid = parseCharGrid(lines, (x, y, v) => {
    return {
      x,
      y,
      height: Number(v),
    };
  });

  if (part === 1) {
    let visible = 0;

    grid.forEach((row, y) => {
      row.forEach((cell, x) => {
        const n = neighbors(grid, x, y);
        if (n.length < 4) {
          visible += 1;
        } else {
          let seen = false;
          let hidden = false;
          const lt = (c: HeightCell): boolean => c.height < cell.height;

          if (toNorth(grid, x, y).every(lt)) {
            seen = true;
          }
          hidden = false;
          0;
          if (toEast(grid, x, y).every(lt)) {
            seen = true;
          }
          hidden = false;
          if (toSouth(grid, x, y).every(lt)) {
            seen = true;
          }
          hidden = false;
          if (toWest(grid, x, y).every(lt)) {
            seen = true;
          }
          if (seen) {
            visible += 1;
          }
        }
      });
    });

    return visible.toString();
  } else {
    let max = 0;
    grid.forEach((row, y) => {
      row.forEach((cell, x) => {
        let done = false;
        const reducer = (acc: number, cur: HeightCell) => {
          if (!done) {
            if (cur.height >= cell.height) {
              done = true;
            }
            return acc + 1;
          }
          return acc;
        };
        const n = toNorth(grid, x, y).reduce(reducer, 0);
        done = false;
        const e = toEast(grid, x, y).reduce(reducer, 0);
        done = false;
        const s = toSouth(grid, x, y).reduce(reducer, 0);
        done = false;
        const w = toWest(grid, x, y).reduce(reducer, 0);
        const score = n * e * s * w;
        if (score > max) {
          max = score;
        }
      });
    });
    return max.toString();
  }
};

const neighbors = (grid: HeightGrid, x: number, y: number): HeightCell[] => {
  const ns: HeightCell[] = [];
  const n = north(grid, x, y);
  const e = east(grid, x, y);
  const s = south(grid, x, y);
  const w = west(grid, x, y);
  if (n) {
    ns.push(n);
  }
  if (e) {
    ns.push(e);
  }
  if (s) {
    ns.push(s);
  }
  if (w) {
    ns.push(w);
  }
  return ns;
};
type Dir = (grid: HeightGrid, x: number, y: number) => HeightCell | undefined;

const north: Dir = (grid: HeightGrid, x: number, y: number) => {
  if (y > 0) {
    return grid[y - 1][x];
  }
};
const east: Dir = (grid: HeightGrid, x: number, y: number) => {
  if (x < grid[0].length - 1) {
    return grid[y][x + 1];
  }
};
const south: Dir = (grid: HeightGrid, x: number, y: number) => {
  if (y < grid.length - 1) {
    return grid[y + 1][x];
  }
};
const west: Dir = (grid: HeightGrid, x: number, y: number) => {
  if (x > 0) {
    return grid[y][x - 1];
  }
};

const toEdge = (
  dir: Dir,
  grid: HeightGrid,
  x: number,
  y: number
): HeightCell[] => {
  let next = dir(grid, x, y);
  if (next) {
    return [next, ...toEdge(dir, grid, next.x, next.y)];
  }
  return [];
};
const toNorth = (grid: HeightGrid, x: number, y: number): HeightCell[] => {
  const cells = toEdge(north, grid, x, y);
  //console.log(cells);
  return cells;
};
const toEast = (grid: HeightGrid, x: number, y: number): HeightCell[] =>
  toEdge(east, grid, x, y);
const toSouth = (grid: HeightGrid, x: number, y: number): HeightCell[] =>
  toEdge(south, grid, x, y);
const toWest = (grid: HeightGrid, x: number, y: number): HeightCell[] =>
  toEdge(west, grid, x, y);

const Day8: Day = {
  task,

  testInput: `30373
  25512
  65332
  33549
  35390`,

  testResult1: "21",
  testResult2: "8",

  result1: "1845",
  result2: "",
};
export { Day8 };
