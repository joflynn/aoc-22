import { Day1 } from "./1";
import { Day10 } from "./10";
import { Day11 } from "./11";
import { Day12 } from "./12";
import { Day13 } from "./13";
import { Day14 } from "./14";
import { Day15 } from "./15";
import { Day18 } from "./18";
import { Day2 } from "./2";
import { Day20 } from "./20";
import { Day21 } from "./21";
import { Day3 } from "./3";
import { Day4 } from "./4";
import { Day5 } from "./5";
import { Day6 } from "./6";
import { Day7 } from "./7";
import { Day8 } from "./8";
import { Day9 } from "./9";

export default [
  null,
  Day1,
  Day2,
  Day3,
  Day4,
  Day5,
  Day6,
  Day7,
  Day8,
  Day9,
  Day10,
  Day11,
  Day12,
  Day13,
  Day14,
  Day15,
  null,
  null,
  Day18,
  null,
  Day20,
  Day21,
];
