import { parse } from "../utils";

type MonkeyItemUpdate = (value: number) => number;
type MonkeyPredicate = (value: number) => number;
type Monkey = {
  id: number;
  queue: number[];
  pre: MonkeyItemUpdate;
  post: MonkeyItemUpdate;
  test: MonkeyPredicate;
  count: number;
};

const DivisiblePattern = /divisible by ([0-9]*)/;
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const rounds = part === 1 ? 20 : 10000;

  //lookahead to get modulo
  let modulo = lines
    .map((x) => {
      const match = x.match(DivisiblePattern);
      if (match && match.length > 1) {
        return Number(match[1]);
      }
      return 0;
    })
    .filter((x) => x)
    .reduce((acc, cur) => acc * cur, 1);

  const monkeys: Monkey[] = [];
  while (lines.length) {
    const monkey = parseMonkey(
      lines,
      part === 1 ? makeUpdate("/", 3) : makeUpdate("%", modulo)
    );
    if (monkey) {
      monkeys.push(monkey);
    }
  }
  for (let i = 0; i < rounds; i += 1) {
    monkeys.forEach((m) => {
      takeTurn(m, monkeys);
    });

    const debug = false;
    if (debug && (i <= 20 || i % 1000 === 0)) {
      console.log(
        i,
        monkeys.map((m) => m.count)
      );
    }
  }
  const counts = monkeys.map((m) => m.count);
  counts.sort((a, b) => b - a);
  return (counts[0] * counts[1]).toString();
};

const takeTurn = (monkey: Monkey, monkeys: Monkey[]) => {
  let i;
  while ((i = monkey.queue.shift())) {
    monkey.count += 1;
    // Monkey inspects an item with a worry level of 79.
    //   Worry level is multiplied by 19 to 1501.
    i = monkey.pre(i);
    //   Monkey gets bored with item. Worry level is divided by 3 to 500.
    i = monkey.post(i);
    //   Current worry level is not divisible by 23.
    const next = monkey.test(i);
    //   Item with worry level 500 is thrown to monkey 3.
    monkeys.find((m) => m.id === next)?.queue.push(i);
  }
};

const parseMonkey = (lines: string[], post: MonkeyItemUpdate): Monkey => {
  const idLine = lines.shift() || "";
  const queueLine = lines.shift() || "";
  const operationLine = lines.shift() || "";
  const testLines = [
    lines.shift() || "",
    lines.shift() || "",
    lines.shift() || "",
  ];
  lines.shift();

  let id = 0;
  let queue = [1];
  let operation = "";
  let arg = 0;
  const idMatch = idLine.match(/Monkey ([0-9]*)/);
  if (idMatch) {
    id = Number(idMatch[1]);
  }
  const queueMatch = queueLine.match(/Starting items: ([0-9, ]*)/);
  if (queueMatch) {
    queue = queueMatch[1].split(", ").map(Number);
  }
  const operationMatch = operationLine.match(/Operation: new = old (.*) (.*)/);
  if (operationMatch) {
    operation = operationMatch[1];
    arg = Number(operationMatch[2]);
    if (operationMatch[2] === "old") {
      operation = "^";
      arg = 2;
    }
  }
  return {
    id,
    queue,
    pre: makeUpdate(operation, arg),
    post,
    test: makeTest(testLines),
    count: 0,
  };
};

const makeUpdate = (op: string, arg: number) => {
  //given rule like `new = old + 3` take old and return new
  return (value: number): number => {
    switch (op) {
      case "+":
        return value + arg;
      case "*":
        return value * arg;
      case "^":
        return Math.pow(value, arg);
      case "%":
        return value % arg;
      case "/":
        return Math.floor(value / arg);
    }
    return value;
  };
};

const makeTest = (rules: string[]) => {
  //given rules like..
  //Test: divisible by 11
  //If true: throw to monkey 4
  //If false: throw to monkey 6
  //test the input value and return the appropriate id

  const numberAtEnd = /.* ([0-9]*)/;
  const divisorMatch = rules.shift()?.match(numberAtEnd);
  let divisor = 1;
  if (divisorMatch) {
    divisor = Number(divisorMatch[1]);
  }
  const trueMatch = rules.shift()?.match(numberAtEnd);
  let trueMonkey = -1;
  if (trueMatch) {
    trueMonkey = Number(trueMatch[1]);
  }
  const falseMatch = rules.shift()?.match(numberAtEnd);
  let falseMonkey = -1;
  if (falseMatch) {
    falseMonkey = Number(falseMatch[1]);
  }

  return (value: number): number => {
    if (value % divisor === 0) {
      return trueMonkey;
    }
    return falseMonkey;
  };
};

const Day11: Day = {
  task,

  testInput: `Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1`,

  testResult1: "10605",
  testResult2: "2713310158",

  result1: "55216",
  result2: "12848882750",
};
export { Day11 };
