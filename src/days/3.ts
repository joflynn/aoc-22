import { parse } from "../utils";

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  testPriority();

  let sum = 0;
  const groups: string[][] = [];
  let group: string[] = [];
  lines.forEach((line, i) => {
    const [a, b] = splitSack(line);

    let found = "";
    let checked = "";
    a.split("").forEach((x) => {
      if (found !== "") {
        return;
      }
      if (checked.match(x)) {
        return;
      }
      if (b.match(x)) {
        found = x;
        return;
      }
      checked += x;
    });
    sum += priority(found);
    group.push(line);
    if (group.length === 3) {
      groups.push(group);
      group = [];
    }
  });
  if (part === 1) {
    return sum.toString();
  } else {
    let sum = 0;
    groups.forEach((g) => {
      const [a, b, c] = g;
      let found = "";
      let checked = "";
      a.split("").forEach((x) => {
        if (found !== "") {
          return;
        }
        if (checked.match(x)) {
          return;
        }
        if (b.match(x) && c.match(x)) {
          found = x;
          return;
        }
        checked += x;
      });
      sum += priority(found);
    });
    return sum.toString();
  }
};

function splitSack(line: string): [string, string] {
  const n = Math.floor(line.length / 2);
  const a = line.substring(0, n);
  const b = line.substring(n);
  return [a, b];
}

function priority(item: string): number {
  const ord = item.charCodeAt(0);
  if (ord < 97) {
    return ord - 64 + 26;
  } else {
    return ord - 96;
  }
}

function testPriority() {
  const expect = [1, 2, 26, 27, 28, 51, 52];
  ["a", "b", "z", "A", "B", "Y", "Z"].forEach((x, i) => {
    if (expect[i] != priority(x)) {
      throw new Error(`Test Priority, expect ${expect[i]} got ${priority(x)}`);
    }
  });
}
const Day3: Day = {
  task,

  testInput: `vJrwpWtwJgWrhcsFMMfFFhFp
  jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
  PmmdzqPrVvPwwTWBwg
  wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
  ttgJtRGJQctTZtZT
  CrZsJsPPZsGzwwsLwLmpwMDw`,

  testResult1: "157",
  testResult2: "70",

  result1: "8493",
  result2: "2252",
};
export { Day3 };
