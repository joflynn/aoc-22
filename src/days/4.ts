import { parse } from "../utils";

type Range = {
  min: number;
  max: number;
  items: number[];
};
type Pair = {
  a: Range;
  b: Range;
};
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const pairs: Pair[] = [];
  let sum = 0;
  lines.forEach((x) => {
    const [a, b] = x.split(",").map(parseRange);
    pairs.push({ a, b });
  });
  if (part === 1) {
    return pairs
      .reduce((prev, cur) => {
        if (covers(cur.a, cur.b) || covers(cur.b, cur.a)) {
          return prev + 1;
        }
        return prev;
      }, 0)
      .toString();
  } else {
    return pairs
      .reduce((prev, cur) => {
        if (overlaps(cur.a, cur.b) || overlaps(cur.b, cur.a)) {
          return prev + 1;
        }
        return prev;
      }, 0)
      .toString();
  }
};
const parseRange = (range: string) => {
  //inclusive range, 3-5
  const [min, max] = range.split("-").map((x) => Number(x));

  //enumerated items, 3,4,5
  const items = [];
  for (let x = min; x <= max; x += 1) {
    items.push(x);
  }
  return { min, max, items };
};

const lenRange = (range: number[]) => {
  //min max, not steps
  return range[1] - range[0] + 1;
};

const covers = (a: Range, b: Range) => {
  //Is a covered by b
  if (a.min >= b.min && a.max <= b.max) {
    return true;
  }
  return false;
};
const overlaps = (a: Range, b: Range) => {
  //Is there any overlap of a on b
  if (overlapsRight(a, b) || overlapsLeft(a, b)) {
    return true;
  }
  return false;
};
const overlapsRight = (a: Range, b: Range) => {
  //e.g. 3-5,5-7
  return a.min >= b.min && a.min <= b.max;
};
const overlapsLeft = (a: Range, b: Range) => {
  //e.g. 5-7,3-5
  return a.max <= b.max && a.max > b.min;
};

const Day4: Day = {
  task,

  testInput: `2-4,6-8
  2-3,4-5
  5-7,7-9
  2-8,3-7
  6-6,4-6
  2-6,4-8`,

  testResult1: "2",
  testResult2: "4",

  result1: "547",
  result2: "843",
};
export { Day4 };
