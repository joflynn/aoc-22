import { CharCell, Grid, ICell } from "../grid";
import { parse } from "../utils";

const Border = {
  x: 20000,
  y: 20,
};
const Wall = "#";
const Sand = "o";
const Debug = false;
const task: DayFunc = (input, part = 1) => {
  const data = parse(input).map((x) => x.trim());

  const lines: Grid<ICell> = data.map((l) => {
    const points = l.split(" -> ");
    return points.map((p) => {
      const pos = p.split(",");
      return { x: Number(pos[0]), y: Number(pos[1]) };
    });
  });

  let bounds = gridBounds(lines);
  if (part === 2) {
    const y = bounds[1].y + 2;
    lines.push([
      { x: bounds[0].x - Border.x, y },
      { x: bounds[1].x + Border.x, y },
    ]);
    bounds = gridBounds(lines);
  }

  const grid = makeGrid(bounds, "-", Border);

  lines.forEach((line) => {
    line.reduce((acc, cur) => {
      drawLine(grid, offset(acc, bounds[0]), offset(cur, bounds[0]), Wall);
      return cur;
    });
  });
  displayGrid(grid);

  const start = { x: 500, y: 0 };

  let full = false;
  let n = 0;
  while (!full) {
    full = dropSand(grid, offset(start, bounds[0]), bounds[1].y + Border.y);
    if (n % 10000 === 0) {
      displayGrid(grid);
    }
    n += 1;
  }
  displayGrid(grid);

  return (n - 1).toString();
};

const dropSand = (
  grid: Grid<CharCell>,
  current: ICell,
  bottom: number
): boolean => {
  let done = false;
  if (!isEmpty(grid, current)) {
    return true;
  }
  while (!done) {
    const d = down(current);
    const dl = downLeft(current);
    const dr = downRight(current);
    drawPoint(grid, current, ".");

    if (isEmpty(grid, d)) {
      current = d;
    } else {
      //check left
      if (isEmpty(grid, dl)) {
        current = dl;
      } else {
        //check right
        if (isEmpty(grid, dr)) {
          current = dr;
        } else {
          done = true;
          drawPoint(grid, current, Sand);
        }
      }
    }
    if (current.y >= bottom) {
      return true;
    }
  }
  return false;
};
const isEmpty = (grid: Grid<CharCell>, cell: ICell): boolean => {
  return !(
    grid[cell.y][cell.x].value === Wall || grid[cell.y][cell.x].value === Sand
  );
};

const gridBounds = (grid: Grid<ICell>): [ICell, ICell] => {
  const tl = {
    x: Infinity,
    y: Infinity,
  };
  const br = {
    x: -Infinity,
    y: -Infinity,
  };

  grid.forEach((row) => {
    row.forEach((cell) => {
      if (cell.x < tl.x) {
        tl.x = cell.x;
      }
      if (cell.x > br.x) {
        br.x = cell.x;
      }
      if (cell.y < tl.y) {
        tl.y = cell.y;
      }
      if (cell.y > br.y) {
        br.y = cell.y;
      }
    });
  });

  return [tl, br];
};

const makeGrid = (
  bounds: [ICell, ICell],
  init: string = " ",
  border: ICell
): Grid<CharCell> => {
  const grid: Grid<CharCell> = [];
  for (let i = bounds[0].y - border.y; i < bounds[1].y + border.y; i += 1) {
    const row: CharCell[] = [];
    for (let j = bounds[0].x - border.x; j < bounds[1].x + border.x; j += 1) {
      row.push({
        x: j,
        y: i,
        value: init,
      });
    }
    grid.push(row);
  }
  return grid;
};

const offset = (point: ICell, tl: ICell) => {
  return {
    x: point.x - tl.x + Border.x,
    y: point.y - tl.y + Border.y,
  };
};

const displayGrid = (grid: Grid<CharCell>) => {
  if (Debug) {
    console.log();
    grid.forEach((row) => {
      console.log(row.map((x) => x.value).join(""));
    });
  }
};
const drawLine = (
  grid: Grid<CharCell>,
  start: ICell,
  end: ICell,
  char: string
) => {
  if (start.x === end.x) {
    //vertical
    const from = start.y < end.y ? start.y : end.y;
    const to = start.y < end.y ? end.y : start.y;
    for (let y = from; y <= to; y += 1) {
      grid[y][start.x].value = char;
    }
  } else if (start.y === end.y) {
    //horizontal
    const from = start.x < end.x ? start.x : end.x;
    const to = start.x < end.x ? end.x : start.x;
    for (let x = from; x <= to; x += 1) {
      grid[start.y][x].value = char;
    }
  } else {
    console.log("diagonal");
  }
};
const drawPoint = (grid: Grid<CharCell>, cell: ICell, char: string) => {
  grid[cell.y][cell.x].value = char;
};

const down = (cell: ICell): ICell => {
  return {
    x: cell.x,
    y: cell.y + 1,
  };
};
const downLeft = (cell: ICell): ICell => {
  return {
    x: cell.x - 1,
    y: cell.y + 1,
  };
};
const downRight = (cell: ICell): ICell => {
  return {
    x: cell.x + 1,
    y: cell.y + 1,
  };
};

const Day14: Day = {
  task,

  testInput: `498,4 -> 498,6 -> 496,6
  503,4 -> 502,4 -> 502,9 -> 494,9`,

  testResult1: "24",
  testResult2: "93",

  result1: "838",
  result2: "27539",
};
export { Day14 };
