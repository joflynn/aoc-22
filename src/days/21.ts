import { parse } from "../utils";

type Operation = {
  operator: string;
  args: string[];
};
type Scalar = {
  value: number;
};
type Monkey = Operation | Scalar;
type Pack = {
  [key: string]: Monkey;
};
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const pack: Pack = {};
  lines.forEach((l) => {
    const mScalar = l.match(/([a-z]*): ([0-9]*)/);
    const mOp = l.match(/([a-z]*): ([a-z]*) (.) ([a-z]*)/);
    if (mScalar) {
      pack[mScalar[1]] = {
        value: Number(mScalar[2]),
      };
    }
    if (mOp) {
      pack[mOp[1]] = {
        operator: mOp[3],
        args: [mOp[2], mOp[4]],
      };
    }
  });

  console.log(pack);
  console.log(pack["root"]);
  if (part === 1) {
    return evaluate(pack, "root").toString();
  } else {
    const root = pack["root"] as Operation;
    console.log("lhs"); //humn
    evaluate(pack, root.args[0]);
    console.log("rhs");
    const target = evaluate(pack, root.args[1]);
    console.log("---");
    //evaluate(pack, root.args[0]);
    return invert(pack, target, root.args[0], "humn", []).toString();
  }
};

const evaluate = (pack: Pack, start: string): number => {
  const monkey = pack[start];
  if (monkey) {
    if ((monkey as Scalar).value) {
      return (monkey as Scalar).value;
    } else if ((monkey as Operation).operator) {
      const op = monkey as Operation;
      const lhs = evaluate(pack, op.args[0]);
      const rhs = evaluate(pack, op.args[1]);
      //console.log(lhs, op.operator, rhs);
      switch (op.operator) {
        case "+":
          return lhs + rhs;
        case "-":
          return lhs - rhs;
        case "*":
          return lhs * rhs;
        case "/":
          if (!rhs) {
            return 0;
          }
          return lhs / rhs;
      }
    }
  }
  return 0;
};
const invert = (
  pack: Pack,
  target: number,
  start: string,
  x: string,
  queue: Operation[]
): number => {
  const monkey = pack[start];
  if (monkey) {
    if ((monkey as Scalar).value) {
      if (start === "humn") {
        console.log(queue);
        let op = queue.shift();
        let cur = target;
        while (op) {
          switch (op.operator) {
            case "+":
              cur += evaluate(pack, op.args[1]);
              break;
            case "-":
              cur -= evaluate(pack, op.args[1]);
              break;
          }
          console.log(op);
          op = queue.shift();
          // if (op) {
          //   let cur: number;
          //   if (op.args[0] === "humn") {
          //     cur = evaluate(pack, op.args[1]);
          //   } else if (op.args[1] === "humn") {
          //     cur = evaluate(pack, op.args[0]);
          //   }
          //   console.log(cur);
          // }
        }
        return target;
      }
      return (monkey as Scalar).value;
    } else if ((monkey as Operation).operator) {
      const op = monkey as Operation;
      let operator = "";
      switch (op.operator) {
        case "+":
          operator = "-";
          break;
        case "-":
          operator = "+";
          break;
        case "*":
          operator = "/";
          break;
        case "/":
          operator = "*";
          break;
      }
      const args = op.args.reverse();
      queue.push({ args, operator });

      const lhs = invert(pack, target, op.args[0], x, queue);
      const rhs = invert(pack, target, op.args[1], x, queue);
    }
  }
  return 0;
};

// invert 150 = x + 100
//     150-100 = x

const Day21: Day = {
  task,

  testInput: `root: pppw + sjmn
  dbpl: 5
  cczh: sllz + lgvd
  zczc: 2
  ptdq: humn - dvpt
  dvpt: 3
  lfqf: 4
  humn: 5
  ljgn: 2
  sjmn: drzm * dbpl
  sllz: 4
  pppw: cczh / lfqf
  lgvd: ljgn * ptdq
  drzm: hmdt - zczc
  hmdt: 32`,

  testResult1: "152",
  testResult2: "301",

  result1: "81075092088442",
  result2: "",
};
export { Day21 };
