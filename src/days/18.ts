import { ICell } from "../grid";
import { parse } from "../utils";

type ZCell = ICell & {
  z: number;
};
type Drop = {
  size: number;
  surfaceArea: number;
  positions: ZCell[];
  merged?: boolean;
};

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  let drops: Drop[] = lines
    .map((l) => {
      const m = l.match(/([0-9]*),([0-9]*),([0-9]*)/);
      if (m) {
        const x = Number(m[1]);
        const y = Number(m[2]);
        const z = Number(m[3]);
        return {
          size: 1,
          surfaceArea: 6,
          positions: [
            {
              x,
              y,
              z,
            },
          ],
        };
      }
    })
    .filter((x) => x) as Drop[];

  if (part === 1) {
    drops.forEach((a) => {
      a.surfaceArea =
        6 - drops.reduce((acc, cur) => acc + sharedEdges(a, cur), 0);
    });
    return drops.reduce((a, c) => a + c.surfaceArea, 0).toString();
  } else {
    drops.forEach((a) => {
      a.surfaceArea =
        5 - drops.reduce((acc, cur) => acc + sharedEdges(a, cur), 0);
    });
    return drops.reduce((a, c) => a + c.surfaceArea, 0).toString();
  }
};

const sharedEdges = (a: Drop, b: Drop): number => {
  let n = 0;
  a.positions.forEach((aa) => {
    b.positions.forEach((bb) => {
      if (manhattan(aa, bb) === 1) {
        n += 1;
      }
    });
  });
  return n;
};
const canMerge = (a: Drop, b: Drop): boolean => {
  const shared = sharedEdges(a, b);
  return !(a.merged || b.merged) && shared > 0;
};
const manhattan = (a: ZCell, b: ZCell): number => {
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z);
};

const Day18: Day = {
  task,

  testInput: `2,2,2
  1,2,2
  3,2,2
  2,1,2
  2,3,2
  2,2,1
  2,2,3
  2,2,4
  2,2,6
  1,2,5
  3,2,5
  2,1,5
  2,3,5`,

  testResult1: "64",
  testResult2: "58",

  result1: "3542",
  result2: "",
};
export { Day18 };
