import { parse } from "../utils";

const KEY = 811589153;
const DEBUG = false;

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const n = lines.length;
  const m = n - 1;
  const list = parseList(lines);

  const queue: ListNode[] = [];
  walk(list, (i) => queue.push(i));

  if (part === 1) {
    debug(list);
    mix(list, queue, m);
    debug(list);
  } else {
    walk(list, (x) => {
      x.value = x.value * KEY;
    });
    debug(list);
    for (let i = 0; i < 10; i += 1) {
      mix(list, queue, m);
      debug(list);
    }
  }

  let cur = list;
  while (cur.value !== 0) {
    if (cur.next) {
      cur = cur.next;
    }
  }
  let sum = 0;
  for (let i = 1; i <= 3000; i += 1) {
    if (cur.next) {
      cur = cur.next;
    }
    if (i === 1000 || i === 2000 || i === 3000) {
      let x = cur.value;
      sum += x;
    }
  }
  return sum.toString();
};

type ListNode = {
  value: number;
  previous?: ListNode;
  next?: ListNode;
};
const parseList = (lines: string[]): ListNode => {
  const first = Number(lines.shift());
  const head: ListNode = {
    value: first,
  };
  head.previous = head;
  head.next = head;
  let cur = head;
  lines.forEach((l) => {
    const value = Number(l);
    insertAfter(cur, value);
    if (cur.next) {
      cur = cur.next;
    }
  });
  return head;
};

const walk = (list: ListNode, cb: (i: ListNode) => void) => {
  cb(list);
  let cur = list.next;
  while (cur && cur !== list) {
    cb(cur);
    cur = cur.next;
  }
};
const walkBackward = (list: ListNode, cb: (i: ListNode) => void) => {
  let cur = list.previous;
  while (cur && cur !== list) {
    cb(cur);
    cur = cur.previous;
  }
  cb(list);
};

const debug = (list: ListNode) => {
  const v: number[] = [];
  const w: number[] = [];
  walk(list, (x) => v.push(x.value));
  if (DEBUG) {
    console.log(v);
  }
};

const insertAfter = (list: ListNode, value: number) => {
  const after = list.next;
  if (!after) {
    return;
  }
  const newNode = {
    value,
    next: after,
    previous: list,
  };
  after.previous = newNode;
  list.next = newNode;
};
const mix = (head: ListNode, queue: ListNode[], modulo: number) => {
  queue.forEach((item) => {
    //console.log("move", item.value);
    move(item, modulo);
  });
};
const move = (node: ListNode, modulo: number) => {
  const v = node.value % modulo;
  const forward = v > 0;

  //pop this node out
  const p = node.previous;
  const n = node.next;
  if (p && n) {
    p.next = n;
    n.previous = p;
  }

  let target = p;
  if (target) {
    for (let i = 0; i < Math.abs(v); i += 1) {
      if (forward && target.next) {
        target = target.next;
      } else if (target.previous) {
        target = target.previous;
      }
    }

    //insert it there
    node.next = target.next;
    node.previous = target;
    if (target.next) {
      target.next.previous = node;
    }
    target.next = node;
  }
};

const Day20: Day = {
  task,

  // testInput: `5
  // 1
  // -1
  // 0
  // 2`,
  testInput: `1
  2
  -3
  3
  -2
  0
  4`,

  testResult1: "3",
  testResult2: "1623178306",

  result1: "15297",
  result2: "",
};
export { Day20 };
