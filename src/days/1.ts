import { parse } from "../utils";

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  const totals: number[] = [];
  let buffer = 0;

  lines.forEach((line) => {
    //push buffer to list on empty lines
    if (line === "") {
      totals.push(buffer);
      buffer = 0;
    } else {
      //otherwise add to buffer
      buffer += Number(line);
    }
  });

  //push final count as well
  if (buffer > 0) {
    totals.push(buffer);
  }

  //Sort totals descending
  totals.sort((a, b) => b - a);

  if (part === 1) {
    return totals[0].toString();
  } else {
    return (totals[0] + totals[1] + totals[2]).toString();
  }
};

const Day1: Day = {
  task,

  testInput: `1000
  2000
  3000
  
  4000
  
  5000
  6000
  
  7000
  8000
  9000
  
  10000`,

  testResult1: "24000",
  testResult2: "45000",

  result1: "67658",
  result2: "200158",
};
export { Day1 };
