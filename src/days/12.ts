import { Grid, ICell, parseCharGrid } from "../grid";
import { parse } from "../utils";

type CharCell = ICell & {
  height: string;
  distance: number;
  visited?: boolean;
};
type CharGrid = Grid<CharCell>;
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const grid = parseCharGrid<CharCell>(lines, (x, y, v) => {
    return {
      x,
      y,
      height: v,
      distance: Infinity,
      visited: false,
    };
  });

  const start = gridFind(grid, "S");
  const end = gridFind(grid, "E");
  if (start === null || end === null) {
    return "";
  }

  if (part === 1) {
    const length = djikstra(grid, start, [end], notMoreThanOneHeigher);
    return length.toString();
  } else {
    const starts = gridFindAll(grid, "a");

    const length = djikstra(grid, end, [start, ...starts], notMoreThanOneLower);
    return length.toString();
  }
};

const height = (char: string): number => {
  if (char === "S") {
    char = "a";
  }
  if (char === "E") {
    char = "z";
  }
  return char.charCodeAt(0);
};
const notMoreThanOneHeigher = (grid: CharGrid, cell: CharCell): CharCell[] => {
  const n = neighbors(grid, cell.x, cell.y);
  const x = n.filter((c) => height(c.height) <= height(cell.height) + 1);
  // console.log(n, cell, x);
  return x;
};
const notMoreThanOneLower = (grid: CharGrid, cell: CharCell): CharCell[] => {
  const n = neighbors(grid, cell.x, cell.y);
  return n.filter((c) => height(c.height) + 1 >= height(cell.height));
};

const djikstra = (
  grid: Grid<CharCell>,
  start: CharCell,
  ends: CharCell[],
  edgeStrategy: (grid: CharGrid, cell: CharCell) => CharCell[]
): number => {
  const unvisited: CharCell[] = [];
  grid.forEach((row) => {
    row.forEach((cell) => {
      unvisited.push(cell);
    });
  });
  start.distance = 0;
  const visit = (cell: CharCell) => {
    //    console.log("visit", cell);
    const neighbors = edgeStrategy(grid, cell);
    //console.log(neighbors);
    neighbors.forEach((n) => {
      if (!n.visited) {
        if (n.distance > cell.distance + 1) {
          //update it
          n.distance = cell.distance + 1;
        }
      }
    });
    cell.visited = true;
  };

  while (unvisited.length > 0) {
    unvisited.sort((a, b) => a.distance - b.distance);
    const cur = unvisited.shift();
    if (cur) {
      visit(cur);
    }
    if (cur && ends.includes(cur)) {
      return cur.distance;
    }
  }
  return 0;
};
const gridFind = (grid: Grid<CharCell>, search: string) => {
  for (let j = 0; j < grid.length; j += 1) {
    const found = grid[j].find((c) => c.height === search);
    if (found) {
      return found;
    }
  }
  return null;
};
const gridFindAll = (grid: Grid<CharCell>, search: string) => {
  const found: CharCell[] = [];
  for (let j = 0; j < grid.length; j += 1) {
    const f = grid[j].find((c) => c.height === search);
    if (f) {
      found.push(f);
    }
  }
  return found;
};

const Day12: Day = {
  task,

  testInput: `Sabqponm
  abcryxxl
  accszExk
  acctuvwj
  abdefghi`,

  testResult1: "31",
  testResult2: "29",

  result1: "408",
  result2: "399",
};
export { Day12 };

export function neighbors<T>(
  grid: Grid<CharCell>,
  x: number,
  y: number
): CharCell[] {
  const ns: CharCell[] = [];
  const n = north(grid, x, y);
  const e = east(grid, x, y);
  const s = south(grid, x, y);
  const w = west(grid, x, y);
  if (n) {
    ns.push(n);
  }
  if (e) {
    ns.push(e);
  }
  if (s) {
    ns.push(s);
  }
  if (w) {
    ns.push(w);
  }
  return ns;
}
type Dir = (grid: Grid<CharCell>, x: number, y: number) => CharCell | undefined;

const north: Dir = (grid: Grid<CharCell>, x: number, y: number) => {
  if (y > 0) {
    return grid[y - 1][x];
  }
};
const east: Dir = (grid: Grid<CharCell>, x: number, y: number) => {
  if (x < grid[0].length - 1) {
    return grid[y][x + 1];
  }
};
const south: Dir = (grid: Grid<CharCell>, x: number, y: number) => {
  if (y < grid.length - 1) {
    return grid[y + 1][x];
  }
};
const west: Dir = (grid: Grid<CharCell>, x: number, y: number) => {
  if (x > 0) {
    return grid[y][x - 1];
  }
};
