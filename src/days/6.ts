import { parse } from "../utils";

const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());
  const stream = lines[0];
  if (!stream) {
    return "";
  }
  if (part === 1) {
    let i = 0;
    while (i < stream.length - 4) {
      const msg = stream.substring(i, i + 4);
      if (startOfPacket(msg)) {
        break;
      }
      i += 1;
    }
    return (i + 4).toString();
  } else {
    let i = 0;
    while (i < stream.length - 14) {
      const msg = stream.substring(i, i + 14);
      if (startOfMessage(msg)) {
        break;
      }
      i += 1;
    }
    return (i + 14).toString();
  }
};

const startOfPacket = (buf: string): boolean => {
  const uniq = new Set(buf.split(""));
  return uniq.size === 4;
};
const startOfMessage = (buf: string): boolean => {
  const uniq = new Set(buf.split(""));
  return uniq.size === 14;
};

const testSupplemental = (part: number): boolean => {
  const cases = [
    { in: "mjqjpqmgbljsphdztnvjfqwrcgsmlb", out: "19" },
    { in: "bvwbjplbgvbhsrlpgdmjqwftvncz", out: "23" },
    { in: "nppdvjthqldpwncqszvftbrmjlhg", out: "23" },
    { in: "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", out: "29" },
    { in: "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", out: "26" },
  ];
  return cases.every((c) => {
    return task(c.in, part) === c.out;
  });
};

const Day6: Day = {
  task,

  testInput: `mjqjpqmgbljsphdztnvjfqwrcgsmlb`,

  testResult1: "7",
  testResult2: "19",

  result1: "1702",
  result2: "3559",

  testSupplemental,
};
export { Day6 };
