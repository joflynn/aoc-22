import { ICell } from "../grid";
import { parse } from "../utils";

type Dir = "R" | "U" | "L" | "D";
type SnakeCmd = {
  dir: Dir;
  mag: number;
};
const task: DayFunc = (input, part = 1) => {
  const lines = parse(input).map((x) => x.trim());

  let h: ICell = {
    x: 0,
    y: 0,
  };
  let t: ICell = {
    x: 0,
    y: 0,
  };
  let trail: ICell[] = Array.from({ length: 9 }, () => ({
    x: 0,
    y: 0,
  }));

  const cmds: SnakeCmd[] = [];
  lines.forEach((l) => {
    const m = l.match(/([URDL]) ([0-9]*)/);
    if (m && m.length > 2) {
      const dir = m[1] || "";
      const mag = Number(m[2]) || 0;

      if (dir === "U" || dir === "R" || dir === "D" || dir === "L") {
        cmds.push({ dir, mag });
      }
    }
  });

  if (part === 1) {
    const set = new Set<string>();
    cmds.forEach((cmd) => {
      for (let i = 0; i < cmd.mag; i += 1) {
        moveHead(cmd.dir, h, t);
        moveTail(cmd.dir, h, t);

        set.add(`(${t.x}, ${t.y})`);
      }
    });
    return set.size.toString();
  } else {
    const set = new Set<string>();
    cmds.forEach((cmd) => {
      for (let i = 0; i < cmd.mag; i += 1) {
        moveHead(cmd.dir, h, t);
        const last = trail.reduce((acc, cur, idx) => {
          moveTail(cmd.dir, acc, cur);
          return cur;
        }, h);
        set.add(`(${last.x}, ${last.y})`);
      }
    });
    return set.size.toString();
  }
};

const moveHead = (dir: Dir, h: ICell, t: ICell) => {
  if (dir === "U") {
    h.y -= 1;
  }
  if (dir === "R") {
    h.x += 1;
  }
  if (dir === "D") {
    h.y += 1;
  }
  if (dir === "L") {
    h.x -= 1;
  }
};

const moveTail = (dir: Dir, h: ICell, t: ICell) => {
  const d = diff(h, t);
  t.x += d.x;
  t.y += d.y;
};

const dist = (h: ICell, t: ICell): number => {
  return Math.sqrt(Math.pow(h.x - t.x, 2) + Math.pow(h.y - t.y, 2));
};

const UP = { x: 0, y: -1 };
const RIGHT = { x: 1, y: 0 };
const DOWN = { x: 0, y: 1 };
const LEFT = { x: -1, y: 0 };

const diff = (h: ICell, t: ICell): ICell => {
  const distance = dist(h, t);
  if (distance < 2) {
    return { x: 0, y: 0 };
  }
  if (distance === 2) {
    //orthogonal
    if (h.x === t.x) {
      if (h.y > t.y) {
        return DOWN;
      }
      return UP;
    } else {
      if (h.x > t.x) {
        return RIGHT;
      }
      return LEFT;
    }
  }
  //diagonal
  let dx, dy: number;
  if (h.y > t.y) {
    //down
    dy = 1;
  } else {
    dy = -1;
  }
  if (h.x > t.x) {
    dx = 1;
  } else {
    dx = -1;
  }
  return { x: dx, y: dy };
};

const testSupplemental = (part: number): boolean => {
  const cases = [
    {
      in: `R 9`,
      out: "1",
    },
    {
      in: `R 5
    U 8
    L 8
    D 3
    R 17
    D 10
    L 25
    U 20`,
      out: "36",
    },
  ];
  return cases.every((c) => {
    const res = task(c.in, part);
    console.log(`Result: ${res}`);
    return res === c.out;
  });
};
const Day9: Day = {
  task,

  testInput: `R 4
  U 4
  L 3
  D 1
  R 4
  D 1
  L 5
  R 2`,

  testResult1: "13",
  testResult2: "1",

  result1: "5710",
  result2: "2259",

  testSupplemental,
};
export { Day9 };
