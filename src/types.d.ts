type DayFunc = (input: string, part: number) => string;

interface Day {
  task: DayFunc;
  testInput: string;
  testResult1: string;
  testResult2?: string;
  result1: string;
  result2: string;

  testSupplemental?: (part: number) => boolean;
}
