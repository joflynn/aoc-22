export type ICell = {
  x: number;
  y: number;
};
export type Grid<Cell> = Cell[][];

export type HeightCell = ICell & {
  height: number;
};

export type FlagCell = ICell & {
  flag?: boolean;
};

export type Rect = {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
};

export function parseCharGrid<Type>(
  lines: string[],
  constructor: (x: number, y: number, value: string) => Type
): Grid<Type> {
  const grid: Grid<Type> = [];
  lines.forEach((line, y) => {
    grid[y] = [];
    line.split("").forEach((value, x) => {
      grid[y][x] = constructor(x, y, value);
    });
  });
  return grid;
}
// export function gridBounds(grid: Grid): Rect {
//   return {
//     x1: 0,
//     x2: grid[0].length,
//   };
// }

export type CharCell = ICell & {
  value: string;
};
