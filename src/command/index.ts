type Command = {
  input: string;
  output: string[];
};

const InputPattern = /^\$ ?/;

const newCommand = (): Command => {
  return { input: "", output: [] };
};

export const parseCli = (lines: string[]): Command[] => {
  const commands: Command[] = [];
  let current: Command = newCommand();
  lines.forEach((line) => {
    if (line.match(InputPattern)) {
      //push previous
      if (current.input || current.output.length) {
        commands.push(current);
        current = newCommand();
      }
      current.input = line.replace(InputPattern, "");
    } else {
      current.output.push(line);
    }
  });
  //push final
  if (current.input || current.output.length) {
    commands.push(current);
  }
  return commands;
};
