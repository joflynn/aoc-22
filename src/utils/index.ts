import fs from "fs";

export function load(num: number) {
  return fs.readFileSync(`./src/data/${num}.txt`, "utf8");
}
export function parse(input: string, splitPattern = "\n") {
  return input.split(splitPattern);
}
