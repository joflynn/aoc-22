export const lcm = (numbers: number[]): number => {
  const gcd2 = (a: number, b: number): number => {
    if (!b) {
      return a;
    }
    return gcd2(b, a % b);
  };
  const lcm2 = (a: number, b: number): number => {
    return (a * b) / gcd2(a, b);
  };

  return numbers.reduce((acc, cur) => lcm2(cur, acc), 1);
};
